var CanvasController = function() {
	
	this.CurrentContext = {};
	this.CurrentCanvas = {};
	this.CurrentBeats = [];
	this.AudioBars = [];
	this.BeatCircles = [];
	
	this.Initialize = function (barCount) {
		var thisCanvas = document.getElementById('waveformValues');
		this.CurrentContext = thisCanvas.getContext('2d');
		this.CurrentCanvas = thisCanvas;

		// Add all bars to the rendering list and add references to them to our AudioBars list
		for (var i = 0; i < barCount; i++) {
			CurrentScene.RenderList.push({
				ObjectType: "Rectangle",
				IsAwaitingUpdate: true,
				FillColor: "#000000",
				Position: {
					X: 0,
					Y: 0
				},
				Size: {
					Width: 0,
					Height: 0
				}
			});
			this.AudioBars.push(CurrentScene.RenderList[CurrentScene.RenderList.length - 1]);
		}
	};
	
	this.ClearContext = function () {
		var c = this.CurrentCanvas;
		this.CurrentContext.clearRect(0, 0, c.width, c.height);
	};
	
	this.DrawBars = function (barList) {
		var c = this.CurrentCanvas;

		var barcount = barList.length;
		var barheight = Math.floor(c.height / barcount);

		for (var i = 0; i < barList.length; i++) {
			var thisBar = barList[i];
			var calcBarWidth = thisBar.Amount / 256 * c.width;
			var greyScale = Math.floor(thisBar.Amount).toString(16);
			var colorSwitch = "#" + greyScale + greyScale + greyScale;

			this.AudioBars[i].Size.Width = calcBarWidth;
			this.AudioBars[i].Size.Height = barheight;
			this.AudioBars[i].Position.X = 10;
			this.AudioBars[i].Position.Y = barheight * i;
		}


	};
	
	this.DrawRect = function (startX, startY, width, height, color) {
		var cc = this.CurrentContext;
		cc.fillStyle = "rgba(" + color.r + "," + color.g + "," + color.b + "," + color.a + ")";
		cc.fillRect(startX, startY, width, height);
	};
	
	this.DrawCircle = function (startX, startY, radius, color) {
		var cc = this.CurrentContext;
		cc.beginPath();
		cc.arc(startX, startY, radius, 0, 2 * Math.PI, false);
		cc.fillStyle = color;
		cc.fill();
		cc.closePath();
	};
	
	this.SetBeats = function (beatList) {
		if (this.CurrentBeats.length == 0) {
			var canvasHeight = this.CurrentCanvas.height;
			var offsetEach = Math.floor(canvasHeight / beatList.length);

			for (var i = 0; i < beatList.length; i++) {
				var thisBeat = {
					X: 200,
					Y: offsetEach * i,
					DecayLeft: beatList[i] ? 255 : 0

				}

				this.CurrentBeats.push(thisBeat);
			}
		}
		else {
			for (var i = 0; i < beatList.length; i++) {
				if (beatList[i]) {
					this.CurrentBeats[i].DecayLeft = 255;
				}
			}
		}

		this.DrawBeats(20);
	};
	
	this.DrawBeats = function (decaySpeed) {

		for (var i = 0; i < this.CurrentBeats.length; i++) {
			var thisBeat = this.CurrentBeats[i];
			thisBeat.DecayLeft -= decaySpeed;
			if (thisBeat.DecayLeft > 0) {
				var sizePercent = thisBeat.DecayLeft / 256;
				this.DrawCircle(thisBeat.X, thisBeat.Y, Math.floor(sizePercent * 120), "FF");
			}
		}
	};
}